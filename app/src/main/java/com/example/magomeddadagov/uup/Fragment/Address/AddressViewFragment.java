package com.example.magomeddadagov.uup.Fragment.Address;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.magomeddadagov.uup.Model.Address;
import com.example.magomeddadagov.uup.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressViewFragment extends Fragment {

    Address address;

    public AddressViewFragment() {
        // Required empty public constructor
    }

    public  AddressViewFragment(Address address)
    {
        this.address=address;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_address_view, container, false);
    }
    @Override
    public  void  onResume()
    {
      super.onResume();



            TextView textView = (TextView) getView().findViewById(R.id.tv_fullAddress);
            textView.setText(address.toString());

    }

}
