package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;
import com.orm.dsl.NotNull;
import com.orm.dsl.Unique;

public  class PreventiveType extends SugarRecord
{
    @Unique
    @NotNull
    String name;

    @Override
    public String toString() {
        return  name ;

    }

    public  PreventiveType(){}
    public  PreventiveType(String name){this .name=name;}
}
