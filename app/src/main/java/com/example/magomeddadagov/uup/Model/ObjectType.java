package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;
import com.orm.dsl.NotNull;
import com.orm.dsl.Unique;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class ObjectType extends SugarRecord {

    @Unique
    @NotNull
    String name;

    public String getName() {
        return  name ;

    }
    public  ObjectType(){}
    public  ObjectType(String name)
    {
        this .name=name;
    }
}
