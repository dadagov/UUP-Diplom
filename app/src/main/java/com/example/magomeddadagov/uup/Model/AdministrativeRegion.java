package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class AdministrativeRegion extends SugarRecord {

    int regionNumber;
    long inspectorId;


    public  int getRegionNumber(){return  regionNumber;}

    public  Inspector getInspector()
    {
        return  Inspector.findById(Inspector.class,inspectorId);
    }
    public  void  setInspector(Inspector inspector){this.inspectorId=inspector.getId();}

    public  AdministrativeRegion() {

    }
    public  AdministrativeRegion(int regionNumber)
    {
        this.regionNumber=regionNumber;
        this.inspectorId=-1;
    }
    public  AdministrativeRegion(int regionNumber, Inspector inspector )
    {
        this.regionNumber=regionNumber;
        this.inspectorId=inspector.getId();
    }
}
