package com.example.magomeddadagov.uup.Fragment.Offence;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.magomeddadagov.uup.Fragment.Street.StreetViewFragment;
import com.example.magomeddadagov.uup.Helper.Helper;
import com.example.magomeddadagov.uup.Model.Offence;
import com.example.magomeddadagov.uup.Model.OffenceType;
import com.example.magomeddadagov.uup.Model.People;
import com.example.magomeddadagov.uup.Model.Street;
import com.example.magomeddadagov.uup.R;

import java.util.ArrayList;


public class OffenceListFragment extends Fragment {


    ArrayList<Offence> offences;
    public OffenceListFragment() {
        offences=(ArrayList<Offence>)Offence.listAll(Offence.class);

    }
    public OffenceListFragment(ArrayList<Offence> offences)
    {
       this.offences=offences;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_offence_list, container, false);
    }

    @Override
    public  void  onResume()
    {
        super.onResume();




        ListView listView=(ListView)getView().findViewById(R.id.lv_offenceList);
        final ArrayAdapter<Offence> adapter=new ArrayAdapter<Offence>(getContext(),android.R.layout.simple_list_item_1,offences );
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Offence offence =(Offence) adapterView.getAdapter().getItem(i);

                FragmentTransaction transaction=getFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.fragment_frame,new OffenceViewFragment(offence));
                transaction.commit();




            }
        });



    }

}
