package com.example.magomeddadagov.uup.Fragment.People;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.magomeddadagov.uup.Adapter.PeopleAdapter;
import com.example.magomeddadagov.uup.Model.People;
import com.example.magomeddadagov.uup.Model.Street;
import com.example.magomeddadagov.uup.R;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class PeopleListFragment extends Fragment {

    ArrayList<People> peoples;
    PeopleAdapter peopleAdapter;

    public PeopleListFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_people_list, container, false);
    }

    @Override
    public  void onResume()
    {
        super.onResume();


        peoples=(ArrayList<People>)People.listAll(People.class);
        peopleAdapter=new PeopleAdapter(getContext(),peoples);
        SearchView searchView=(SearchView)getView().findViewById(R.id.sv_people);
        searchView.setQueryHint("Поиск");

        ListView listView=(ListView)getView().findViewById(R.id.lv_peopleList);
        listView.setAdapter(peopleAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        People people=(People) adapterView.getAdapter().getItem(i);

        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_frame,new PeopleViewFragment(people));
        transaction.commit();


    }
});


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Search(s);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
              Search(s);
                return false;
            }
        });


        FloatingActionButton actionButton=(FloatingActionButton)getView().findViewById(R.id.fab_peopleListAdd);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction=getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_frame, new PeopleViewFragment(),null);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });



    }


    private void  Search(String s)
    {
        String[] queryes=s.split(" ");

        Select<People> query = Select.from(People.class);

        for (String q:queryes)
        {
            q=q+"%";

            q=q.substring(0,1).toUpperCase()+q.substring(1).toLowerCase();


            query.where(Condition.prop("name").like(q))
                    .or(Condition.prop("surname").like(q))
                    .or(Condition.prop("middlename").like(q));

        }
        peoples.clear();
        peoples.addAll( query.list());
        peopleAdapter.notifyDataSetChanged();


    }

}
