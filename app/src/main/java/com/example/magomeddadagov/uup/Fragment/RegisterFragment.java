package com.example.magomeddadagov.uup.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.magomeddadagov.uup.Model.Inspector;
import com.example.magomeddadagov.uup.R;

import java.util.List;


public class RegisterFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {




        return inflater.inflate(R.layout.fragment_register, container, false);
    }


    @Override
    public  void  onResume()
    {
        super.onResume();

        final EditText et_emailReg=(EditText)getView().findViewById(R.id.et_emailReg);

        final EditText et_pswdReg=(EditText)getView().findViewById(R.id.et_pswdReg);
        final EditText et_pswdConfReg=(EditText) getView().findViewById(R.id.et_pswdConfirmReg);
        final EditText et_nameReg=(EditText)getView().findViewById(R.id.et_nameReg);
        final EditText et_surnameReg=(EditText)getView().findViewById(R.id.et_surnameReg);
        final EditText et_middlenameReg=(EditText)getView().findViewById(R.id.et_middlenameReg);
        final EditText et_phoneReg=(EditText)getView().findViewById(R.id.et_phoneReg);








        final Button btn_create=(Button)getView().findViewById(R.id.btn_createAccaunt);



        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String login=et_emailReg.getText().toString();

                String pswd=et_pswdReg.getText().toString();
                String pswdConf=et_pswdConfReg.getText().toString();

                String name=et_nameReg.getText().toString();
                String surname=et_surnameReg.getText().toString();
                String middlename=et_middlenameReg.getText().toString();
                String phone=et_phoneReg.getText().toString();


                if (login.length()==0)
                {
                    Toast.makeText(getContext(), "Введите логин", Toast.LENGTH_SHORT).show();
                    return;
                }


                List<Inspector> inspectors= Inspector.find(Inspector.class,"login = ?", login);

                if ( inspectors.size()>0)
                {
                    Toast.makeText(getContext(), "Логин уже занят", Toast.LENGTH_SHORT).show();
                    return;
                }



                if (pswd.length()==0)
                {
                    Toast.makeText(getContext(), "Введите пароль", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( !pswd.equals(pswdConf))
                {
                    Toast.makeText(getContext(), "Не верный повторный пароль", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (name.length()==0)
                {
                    Toast.makeText(getContext(), "Введите имя", Toast.LENGTH_SHORT).show();
                    return;
                }
                if ( surname.length()==0)
                {
                    Toast.makeText(getContext(), "Введите фамилию", Toast.LENGTH_SHORT).show();
                    return;
                }





                Inspector inspector=new Inspector(name,surname,middlename,phone,login, pswd);
                inspector.save();
                Toast.makeText(getContext(), "Аккаунт создан успешно!", Toast.LENGTH_SHORT).show();

                FragmentTransaction transaction=getActivity(). getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.fragment_frame,new LoginFragment(), null);

                transaction.commit();



            }
        });


    }



}
