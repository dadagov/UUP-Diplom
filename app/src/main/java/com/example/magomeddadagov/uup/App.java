package com.example.magomeddadagov.uup;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by Dadagov on 16.01.2017.
 */

public class App extends Application {
  //  private static App.AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        //component = App.DaggerAppComponent.create();
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

   // public static App.AppComponent getComponent(){
     // return component;
    //}
}
