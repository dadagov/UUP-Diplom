package com.example.magomeddadagov.uup.Repository.Interface;

import java.util.Collection;



public interface Repository<T, I> {
    T getById(I id);

    I create(T entity);

    I save(T entity);

    boolean delete(T entity);

    int deleteAll();

    Collection<T> getAll();

    T getFirst();

    T getLast();

    int count();

    boolean exist(I id);



}
