package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;
import com.orm.dsl.NotNull;
import com.orm.dsl.Unique;


public class Street extends SugarRecord {

    @Unique
    @NotNull
    String name;
    public  Street(){}
    public  Street(String name)
    {
        this.name=name;
    }


    @Override
    public String toString()
    {
        return  name;
    }

}
