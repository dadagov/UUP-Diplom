package com.example.magomeddadagov.uup.Helper;

import android.content.Context;
import android.text.format.DateUtils;

import com.example.magomeddadagov.uup.MainActivity;
import com.example.magomeddadagov.uup.Model.Address;
import com.example.magomeddadagov.uup.Model.ObjectType;
import com.example.magomeddadagov.uup.Model.Offence;
import com.example.magomeddadagov.uup.Model.OffenceType;
import com.example.magomeddadagov.uup.Model.OrgObject;
import com.example.magomeddadagov.uup.Model.People;
import com.example.magomeddadagov.uup.Model.PreventiveType;
import com.example.magomeddadagov.uup.Model.Street;
import com.example.magomeddadagov.uup.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by magomeddadagov on 17.11.16.
 */

public class Helper {

    public  static Date createDate(String ddMMyyyy)
    {


        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        try {
           return dateFormat.parse(ddMMyyyy);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public  static  void  initDb(Context context)
    {
        for (String item :context.getResources().getStringArray(R.array.ObjcetType))
        {
            ObjectType type=new ObjectType(item);
            type.save();

        }

        for(String item:context.getResources().getStringArray(R.array.preventiveType))
        {
            PreventiveType preventiveType=new PreventiveType(item);
            preventiveType.save();
        }


        Street street=new Street("Кадырова");
        street.save();
        Address address=new Address(street,2,23);
        address.save();

        People people=new People("Магомед", "Алал","Ву", Calendar.getInstance().getTimeInMillis(),"Саратовская область",address);
        people.save();

//OrgObject.deleteAll(OrgObject.class);
        OrgObject orgObject =new OrgObject("OOO GoTech", address,"Иван","9280048881",ObjectType.first(ObjectType.class),"Security", "Other");
        //orgObject.save();

        //Offence.deleteAll(Offence.class);
        Offence offence=new Offence(OffenceType.KoAP,Helper.createDate("21/11/2016"),"Москва",people,"20.25","Штраф 500","Совершил нарушение");
        //offence.save();
    }

    public  static  String getTextDate(long milis)
    {
      return   DateUtils.formatDateTime(MainActivity.Instance,milis,DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR);
    }
}
