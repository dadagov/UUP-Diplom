package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class Preventive extends SugarRecord {

    long peopleId;
    long typeId;
    Date startDate;
    Date endDate;
    Date openClosedDate;
    Boolean isClosed=false;

    long startReasonId;
    long endReasonId;

    long offenceId;

    public Preventive(){

    }

    public  Preventive(People people, PreventiveType type, Date startDate, Date endDate, StartReason startReason, EndReason endReason, Offence offence)
    {
        this.peopleId=people.getId();
        this.typeId=type.getId();
        this.startDate=startDate;
        this.endDate=endDate;
        this.startReasonId=startReason.getId();
        this.endReasonId=endReason.getId();
        this.offenceId=offence.getId();
    }


    public  Preventive(People people, PreventiveType type, Date startDate,  StartReason startReason,  Offence offence) {
        this.peopleId = people.getId();
        this.typeId = type.getId();
        this.startDate = startDate;
        this.startReasonId = startReason.getId();
        this.offenceId = offence.getId();
    }


    public  Boolean IsClosed()
    {return  isClosed;}

    public void  Close()
    {
        openClosedDate =new Date();
        isClosed=true;
        save();
    }

    public  void Open()
    {
        openClosedDate =new Date();
        isClosed=false;
        save();

    }







}

