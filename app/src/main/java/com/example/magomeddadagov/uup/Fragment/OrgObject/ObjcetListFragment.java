package com.example.magomeddadagov.uup.Fragment.OrgObject;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.magomeddadagov.uup.Fragment.People.PeopleViewFragment;
import com.example.magomeddadagov.uup.Fragment.Street.StreetViewFragment;
import com.example.magomeddadagov.uup.Model.OrgObject;
import com.example.magomeddadagov.uup.Model.Street;
import com.example.magomeddadagov.uup.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ObjcetListFragment extends Fragment {

ArrayList<OrgObject> orgObjects;
    public ObjcetListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_objcet_list, container, false);
    }


    @Override
    public void  onResume()
    {
        super.onResume();

        orgObjects=(ArrayList<OrgObject>) OrgObject.listAll(OrgObject.class);

        ArrayAdapter<OrgObject> arrayAdapter=new ArrayAdapter<OrgObject>(getContext(),android.R.layout.simple_list_item_1, orgObjects );

        ListView listView=(ListView)getView().findViewById(R.id.lv_objectList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                OrgObject orgObject =(OrgObject) adapterView.getAdapter().getItem(i);

                FragmentTransaction transaction=getFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.fragment_frame,new ObjectViewFragment(orgObject));
                transaction.commit();
            }
        });

        FloatingActionButton actionButton=(FloatingActionButton)getView().findViewById(R.id.fab_objectListAdd);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction=getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_frame, new ObjectViewFragment(),null);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });



    }




}
