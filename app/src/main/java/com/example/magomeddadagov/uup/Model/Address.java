package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;
import com.orm.dsl.NotNull;
import com.orm.dsl.Unique;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class Address extends SugarRecord {
    long streetId;
    int house;
    String housing;
    int apartment;
    @Unique
    @NotNull
    String fullAddress;

    public  Street getStreet()
    {
       return Street.findById(Street.class,streetId);
    }

    public  int getHouse(){return house;}

    public  String getHousing(){return  housing;}

    public  int getApartment(){return  apartment;}



    public  Address()
    {

    }
    public  Address(Street street, int house, String housing, int apartment)
    {
        this.streetId=street.getId();
        this.house=house;
        this.housing=housing;
        this.apartment=apartment;

      StringBuilder builder=new StringBuilder();
        builder.append("ул. ");
        builder.append(getStreet());
        builder.append(", д. ");
        builder.append(house);
        if (housing!=null)
        {
            builder.append(", корп. ");
            builder.append(housing);
        }
        if (apartment>0)
        {
            builder.append(", кв. ");
            builder.append(apartment);
        }
        this.fullAddress=builder.toString();


    }
    public  Address(Street street, int house,  int apartment)
    {
        this(street,house,null,apartment);

    }
    public  Address(Street street, int house, String housing)
    {
        this(street,house,housing,0);
    }

    public  Address(Street street, int house) {
        this(street, house, null, 0);
    }


    @Override
    public  String toString()
    {
       return fullAddress;
    }


}
