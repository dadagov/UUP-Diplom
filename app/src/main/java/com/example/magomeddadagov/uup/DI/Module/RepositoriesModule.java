package com.example.magomeddadagov.uup.DI.Module;

import com.example.magomeddadagov.uup.Repository.Interface.StreetRepository;
import com.example.magomeddadagov.uup.Repository.StreetRepositoryImpl;
import com.orm.SugarRecord;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class RepositoriesModule {



    @Provides
    public StreetRepository provideStreetRepository () {
           return new StreetRepositoryImpl();
    }


}
