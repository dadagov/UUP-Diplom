package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.Date;



public class Offence extends SugarRecord {

    Date date;
    String place;
    long peopleId;
    String article;
    String sanction;
    String description;
    OffenceType type;


    public Date getDate(){return  date;}
    public String getPlace(){return  place;}
    public  People getPeople() {return  People.findById(People.class,peopleId);}
    public  String getArticle(){return  article;}
    public  String getSanction(){return  sanction;}
    public  String getDescription(){return  description;}
    public  OffenceType getType(){return  type;}





    public  Offence(){
    }

    public  Offence(OffenceType type , Date date, String place, People people, String article, String sanction, String description)
    {
        this.type=type;
        this    .date=date;
        this    .place=place;
        this.peopleId=people.getId();
        this.article=article;
        this   .sanction=sanction;
        this    .description=description;

    }

    @Override
    public  String toString()
    {
        return date.toLocaleString()+", "+place+", "+getPeople()+", "+article;
    }



}

