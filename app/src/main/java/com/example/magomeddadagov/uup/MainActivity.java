package com.example.magomeddadagov.uup;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.magomeddadagov.uup.Fragment.Address.AddressListFragment;
import com.example.magomeddadagov.uup.Fragment.LoginFragment;
import com.example.magomeddadagov.uup.Fragment.MainFragment;
import com.example.magomeddadagov.uup.Fragment.Offence.OffenceListFragment;
import com.example.magomeddadagov.uup.Fragment.OrgObject.ObjcetListFragment;
import com.example.magomeddadagov.uup.Fragment.People.PeopleListFragment;
import com.example.magomeddadagov.uup.Fragment.RegisterFragment;
import com.example.magomeddadagov.uup.Fragment.Street.StreetListFragment;
import com.example.magomeddadagov.uup.Helper.Helper;
import com.example.magomeddadagov.uup.Model.Inspector;
import com.example.magomeddadagov.uup.Model.People;

import java.util.Date;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    public static MainActivity Instance;
    private static Inspector Inspector=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_main);

        Instance=this;
      // Helper.initDb(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle( this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setTitle("Main");


        //////////////////
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();

        //transaction.replace(R.id.fragment_frame,new LoginFragment(), null);
        //transaction.replace(R.id.fragment_frame, new PeopleListFragment(),null);
        //transaction.replace(R.id.fragment_frame, new StreetListFragment(),null);
        //transaction.replace(R.id.fragment_frame,new AddressListFragment(), null);
        //transaction.replace(R.id.fragment_frame,new OffenceListFragment(),null);
       // transaction.replace(R.id.fragment_frame,new ObjcetListFragment());
        transaction.replace(R.id.fragment_frame,new MainFragment());
        //transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override //sittings
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }







    public static  Inspector getInspector(){
        return  Inspector;
    }
    public  static void  setInspector(Inspector inspector){
        Inspector=inspector;
    }


    @Override
    public void onClick(View view) {
        int id=view.getId();
        final FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();


        switch (id)
        {
            case R.id.btn_mainPeople:
                 transaction.replace(R.id.fragment_frame, new PeopleListFragment(),null);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case  R.id.btn_mainObjects:
                transaction.replace(R.id.fragment_frame, new ObjcetListFragment(),null);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.btn_mainPreventive:
                break;
            case R.id.btn_mainAdminOffence:

                transaction.replace(R.id.fragment_frame, new OffenceListFragment(),null);
                transaction.addToBackStack(null);
                transaction.commit();

                break;
            case R.id.btn_mainCriminalOffence:
                break;
            case R.id.btn_mainStreetAddrss:

                final AlertDialog.Builder builder = new AlertDialog.Builder(this);


                LayoutInflater li = getLayoutInflater();
                View promptsView = li.inflate(R.layout.dialog_street_address, null);
                builder.setView(promptsView);
                final AlertDialog dialog=builder.create();


                Button btn_showAdrs=(Button)promptsView.findViewById(R.id.btn_showAdrs);
                btn_showAdrs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        transaction.replace(R.id.fragment_frame, new AddressListFragment(),null);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        dialog.dismiss();

                    }
                });
                Button btn_showStreets=(Button)promptsView.findViewById(R.id.btn_showStreets);
                btn_showStreets.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        transaction.replace(R.id.fragment_frame, new StreetListFragment(),null);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;
        }

    }





}
