package com.example.magomeddadagov.uup.Entity;

import com.orm.dsl.NotNull;
import com.orm.dsl.Unique;

/**
 * Created by Dadagov on 17.01.2017.
 */

public class Street {
    private Long id=null;
    @Unique
    @NotNull
    private String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
