package com.example.magomeddadagov.uup.Model;

import com.google.common.base.Strings;
import com.orm.SugarRecord;
import com.orm.dsl.NotNull;
import com.orm.dsl.Unique;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class Inspector extends SugarRecord {

    @NotNull
    String name;
    @NotNull
    String surname;
    String middlename;
    String phone;
    @Unique
            @NotNull
    String login;
    @NotNull
    String password;


    public String getName(){return  name;}

    public String getSurname(){return  surname;}

    public String getMiddlename(){return  middlename;}

    public  String getPhone(){return  phone;}
    public  void  setPhone(String phone){
       this  . phone=phone;
    }

    public  String getLogin(){return  login;}
    public  void  setLogin(String login){this.login=login;}

    public  String getPassword(){return  password;}
    public  void  setPassword(String password){this.password=password;}

    public Inspector(){}

    public  Inspector(String name, String surname, String middlename, String phone, String login, String password )
    {
        this    .name=name;
        this    .surname=surname;
        this.middlename=middlename!=null?middlename: "";
        this .phone=phone;
        this    .login=login;
        this    .password=password;

    }
    public  Inspector(String name, String surname, String middlename,  String login, String password )
    {
        this(name,surname,middlename,"",login,password);

    }





}
