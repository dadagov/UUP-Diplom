package com.example.magomeddadagov.uup.Repository;

import com.example.magomeddadagov.uup.Entity.Street;
import com.example.magomeddadagov.uup.Repository.Interface.StreetRepository;
import com.google.common.collect.Lists;
import com.orm.SugarRecord;

import org.abego.treelayout.internal.util.java.lang.IterableUtil;
import org.abego.treelayout.internal.util.java.util.IteratorUtil;
import org.abego.treelayout.internal.util.java.util.ListUtil;

import java.util.Collection;
import java.util.Collections;


public class StreetRepositoryImpl implements StreetRepository {


    @Override
    public Street getById(Long id) {
        if (id < 1) return null;
        return SugarRecord.findById(Street.class, id);
    }

    @Override
    public Long create(Street entity) {
        if (entity == null) return null;
        if (this.exist(entity.getId())) return null;
        return SugarRecord.save(entity);
    }

    @Override
    public Long save(Street entity) {
        if (entity == null) return null;
        if (this.exist(entity.getId())) {
            return SugarRecord.save(entity);
        }
        return null;
    }

    @Override
    public boolean delete(Street entity) {
        if (entity == null) return false;
        if (!this.exist(entity.getId())) return false;
        return SugarRecord.delete(entity);

    }

    @Override
    public int deleteAll() {
        return SugarRecord.deleteAll(Street.class);
    }

    @Override
    public Collection<Street> getAll() {
        return Lists.newArrayList(SugarRecord.findAll(Street.class));
    }

    @Override
    public Street getFirst() {
        return SugarRecord.first(Street.class);
    }

    @Override
    public Street getLast() {
        return SugarRecord.last(Street.class);
    }

    @Override
    public int count() {
        return (int) SugarRecord.count(Street.class);
    }

    @Override
    public boolean exist(Long id) {
        if (id < 1) return false;
        if (SugarRecord.findById(Street.class, id) != null) {
            return true;
        }
        return false;
    }


}
