package com.example.magomeddadagov.uup.Fragment.Offence;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.magomeddadagov.uup.Fragment.People.PeopleViewFragment;
import com.example.magomeddadagov.uup.Model.Offence;
import com.example.magomeddadagov.uup.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OffenceViewFragment extends Fragment {

Offence offence;
    public OffenceViewFragment() {
        // Required empty public constructor
    }

    public  OffenceViewFragment(Offence offence)
    {
      this.offence=offence;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_offence_view, container, false);
    }

    @Override
    public  void onResume()
    {
        super.onResume();



        TextView tv_offenceDate=(TextView)getView().findViewById(R.id.tv_offenceDate);
        TextView tv_offencePlace=(TextView)getView().findViewById(R.id.tv_offencePlace);
        TextView tv_offenceArticle=(TextView)getView().findViewById(R.id.tv_offenceArticle);
        TextView tv_offenceSanc=(TextView)getView().findViewById(R.id.tv_offenceSanc);
        TextView tv_offenceDesc=(TextView)getView().findViewById(R.id.tv_offenceDesc);
        TextView tv_offenceType=(TextView)getView().findViewById(R.id.tv_offenceType);

        tv_offenceDate.setText(offence.getDate().toLocaleString());
        tv_offencePlace.setText(offence.getPlace());
        tv_offenceArticle.setText(offence.getArticle());
        tv_offenceSanc.setText(offence.getSanction());
        tv_offenceDesc.setText(offence.getDescription());
        tv_offenceType.setText(offence.getType().toString());


        FragmentTransaction transaction=getActivity(). getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.frame_offencePeople,new PeopleViewFragment(offence.getPeople()),null);

        transaction.commit();
    }


}
