package com.example.magomeddadagov.uup.Model;

import com.orm.SugarRecord;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class EndReason extends SugarRecord {
    String name;

    public String getName() {
        return  name ;

    }

    public  EndReason(){}
    public  EndReason(String name){this.name=name;}
}
