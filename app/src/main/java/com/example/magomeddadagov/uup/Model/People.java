package com.example.magomeddadagov.uup.Model;

import android.text.format.DateUtils;

import com.example.magomeddadagov.uup.Helper.Helper;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.Date;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class People extends SugarRecord {

    long addressId;
    String name;
    String surname;
    String middlename;



    long birthDate;
    String birthPlace;

    @Unique
    String fullnameWithBirth;

    String fullName;


    public  Address getAddress()
    {
       return Address.findById(Address.class,addressId);
    }
    public  void  setAddress(Address address)
    {
        addressId=address.getId();
    }

    public String getName(){return  name;}

    public String getSurname(){return  surname;}

    public String getMiddlename(){return  middlename;}
    public  long getBirthDate(){return birthDate;}
    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
    public String getBirthPlace(){return  birthPlace;}



    public  People()
    {

    }

    public People(String name, String surname, String middlename, long birthDate, String birthPlace, Address address) {

        this.name = name;
        this.surname = surname;
        this.middlename = middlename;
        this.birthDate = birthDate;
        this.birthPlace = birthPlace;
        this.addressId = address.getId();
        StringBuilder builder = new StringBuilder();
        builder.append(surname);
        builder.append(" ");
        builder.append(name);

        if (middlename != null && !middlename.isEmpty()) {
          builder.append(" ");
            builder.append(middlename);
        }
        fullName=builder.toString();
        builder.append(" ");
        builder.append(Helper.getTextDate(birthDate));
        builder.append(" ");
        builder.append(birthPlace);
        fullnameWithBirth=builder.toString();
    }

    @Override
    public  String toString()
    {
        return  fullName;
    }


    @Override
    public long save()
    {


        return super.save();
    }




    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setMiddlename(String middleName) {
        this.middlename = middleName;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }
}
