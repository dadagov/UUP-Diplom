package com.example.magomeddadagov.uup.Fragment.Address;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.magomeddadagov.uup.Fragment.Street.StreetViewFragment;
import com.example.magomeddadagov.uup.Model.Address;
import com.example.magomeddadagov.uup.Model.Street;
import com.example.magomeddadagov.uup.R;

import java.util.ArrayList;
import java.util.List;


public class AddressListFragment extends Fragment {

    ArrayList<Address> addresses;
    ArrayAdapter<Address> adapter;

    public AddressListFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_address_list, container, false);
    }


    @Override
    public  void  onResume()
    {
        super.onResume();


        addresses=(ArrayList<Address>)Address.listAll(Address.class);
        ListView listView=(ListView)getView().findViewById(R.id.lv_addressList);

        adapter=new ArrayAdapter<Address>(getContext(),android.R.layout.simple_list_item_1, addresses );
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Address address =(Address) adapterView.getAdapter().getItem(i);

                FragmentTransaction transaction=getFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.fragment_frame,new AddressViewFragment(address));
                transaction.commit();
            }
        });


        FloatingActionButton actionButton=(FloatingActionButton)getView().findViewById(R.id.fab_adrsListAdd);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               // AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                //builder.setView(R.layout.fragment_address_add);

                //builder.show();
                LayoutInflater li = getActivity().getLayoutInflater();

                View promptsView = li.inflate(R.layout.fragment_address_add, null);

                final Spinner spinner = (Spinner)promptsView.findViewById(R.id.sp_adrAdd);
                final EditText et_adrAddHouse=(EditText)promptsView.findViewById(R.id.et_adrAddHouse);
                final EditText et_adrAddHousing=(EditText)promptsView.findViewById(R.id.et_adrAddHousing);
                final EditText et_adrAddApart=(EditText)promptsView.findViewById(R.id.et_adrAddApart);







                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setView(promptsView);
                builder.setTitle("Адрес");
                builder
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                //отправляем результат обратно
                                try {
                                    String selectedItem = (String) spinner.getSelectedItem();
                                    Street street = Street.find(Street.class, "name = ?", selectedItem).get(0);
                                    Address address = new Address(street,
                                            Integer.parseInt(et_adrAddHouse.getText().toString()),
                                            et_adrAddHousing.getText().toString(),
                                            Integer.parseInt(et_adrAddApart.getText().toString()));

                                    address.save();
                                    addresses.add(address);
                                    adapter.notifyDataSetChanged();
                                }
                                catch (Exception ex)
                                {
                                    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setTitle("Адрес");


                List<Street> streets = Street.listAll(Street.class);

                String[] data =new String[streets.size()];
                for (  int i=0; i<data.length; i++)
                {
                    data[i]=streets.get(i).toString();
                }
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, data);
                spinner.setAdapter(adapter);


                builder.show();




            }
        });


    }



}
