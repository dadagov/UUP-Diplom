package com.example.magomeddadagov.uup.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.magomeddadagov.uup.Model.People;
import com.example.magomeddadagov.uup.R;

import java.util.ArrayList;

/**
 * Created by magomeddadagov on 17.11.16.
 */

public class PeopleAdapter extends BaseAdapter {

    ArrayList<People> peoples;
    Context ctx;
    LayoutInflater lInflater;

    public  PeopleAdapter(Context context, ArrayList<People> peoples)
    {
        ctx=context;
        this.peoples=peoples;
        lInflater=(LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return peoples.size();
    }

    @Override
    public Object getItem(int i) {
        return peoples.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.people_list_adapter_tem, viewGroup, false);
        }

        People people=(People) getItem(i);
        TextView tv_peopleName=(TextView)view.findViewById(R.id.tv_people_name);
        tv_peopleName  .setText(people.getName());


        TextView tv_peopleSurname=(TextView)view.findViewById(R.id.tv_people_surname);
        tv_peopleSurname.setText(people.getSurname());

        TextView tv_peopleMiddlename=(TextView)view.findViewById(R.id.tv_people_middlename);
        tv_peopleMiddlename.setText(people.getMiddlename());

        return view;
    }
}
