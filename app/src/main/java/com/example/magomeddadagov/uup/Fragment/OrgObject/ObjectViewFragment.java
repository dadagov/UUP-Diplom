package com.example.magomeddadagov.uup.Fragment.OrgObject;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.magomeddadagov.uup.Model.OrgObject;
import com.example.magomeddadagov.uup.R;

public class ObjectViewFragment extends Fragment {


    OrgObject orgObject;
    public ObjectViewFragment() {
        // Required empty public constructor
    }

    public  ObjectViewFragment(OrgObject orgObject)
    {
        this.orgObject=orgObject;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_object_view, container, false);
    }


    @Override
    public  void  onResume()
    {
        super.onResume();


        TextView tv_objectName=(TextView)getView().findViewById(R.id.tv_objectName);
        TextView tv_objectAddress=(TextView)getView().findViewById(R.id.tv_objectAddress);
        TextView tv_objectOwner=(TextView)getView().findViewById(R.id.tv_objectOwner);

        if (orgObject!=null) {
            tv_objectName.setText(orgObject.getName());
            tv_objectAddress.setText(orgObject.getAddress().toString());
            tv_objectOwner.setText(orgObject.getOwnerName());
        }
    }

}
