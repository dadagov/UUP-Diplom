package com.example.magomeddadagov.uup.Fragment;



import android.app.ProgressDialog;

import android.support.design.widget.NavigationView;

import android.support.v4.app.Fragment;
import  android.support.v4.app.FragmentTransaction;

import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.magomeddadagov.uup.MainActivity;
import com.example.magomeddadagov.uup.Model.Inspector;
import com.example.magomeddadagov.uup.R;

import java.util.List;


public class LoginFragment extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_login, container, false);
        final Fragment currentFragment=this;


        final EditText et_email=(EditText)view.findViewById(R.id.et_email);

        final EditText et_pswd=(EditText)view.findViewById(R.id.et_pswd);

        final Button btn_login=(Button) view.findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login=et_email.getText().toString();
                String password=et_pswd.getText().toString();

               List<Inspector> inspectors= Inspector.find(Inspector.class,"login = ? and password = ?", login,password);

                for(Inspector item: inspectors)
                {
                    if (item.getLogin().equals(login) && item.getPassword().equals(password))
                    {

                        FragmentTransaction transaction=getActivity(). getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_frame,new MainFragment(), null);

                        transaction.commit();
                        MainActivity.setInspector(item);

                        return;
                    }

                }
                Toast.makeText(getContext(), "Неверный логин или пароль", Toast.LENGTH_SHORT).show();


            }
        });

        final Button btn_reg=(Button) view.findViewById(R.id.btn_regist);
        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction=getActivity(). getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.fragment_frame,new RegisterFragment(), null);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        setRetainInstance(true);

        return view;
    }



    @Override
    public  void onResume()
    {
        super.onResume();


    }

}
