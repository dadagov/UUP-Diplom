package com.example.magomeddadagov.uup.Fragment.Street;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.example.magomeddadagov.uup.Fragment.People.PeopleViewFragment;
import com.example.magomeddadagov.uup.Model.Street;
import com.example.magomeddadagov.uup.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class StreetListFragment extends Fragment {

    ArrayList<Street> streets;

    public StreetListFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        return inflater.inflate(R.layout.fragment_street_list, container, false);
    }



    @Override
    public  void onResume()
    {
        super.onResume();



        streets=(ArrayList<Street>)Street.listAll(Street.class);
        ListView listView=(ListView)getView().findViewById(R.id.lv_streetList);
        final ArrayAdapter<Street> adapter=new ArrayAdapter<Street>(getContext(),android.R.layout.simple_list_item_1,streets );
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
         public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Street street =(Street)adapterView.getAdapter().getItem(i);

        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_frame,new StreetViewFragment(street));
        transaction.commit();

            }
        });

        FloatingActionButton floatingButton =(FloatingActionButton) getView().findViewById(R.id.fab_streetListAdd);
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Введите название улицы");


                final EditText input = new EditText(getContext());

                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);



                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Street street=new Street(input.getText().toString());
                        street.save();
                        streets.add(street);
                        adapter.notifyDataSetChanged();

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });

    }
}
