package com.example.magomeddadagov.uup.Fragment.Street;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.magomeddadagov.uup.Model.Street;
import com.example.magomeddadagov.uup.R;


public class StreetViewFragment extends Fragment {

    Street street;

    public StreetViewFragment() {

    }

    public StreetViewFragment(Street street)
    {
       this.street=street;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_street_view, container, false);
    }

    @Override
    public  void  onResume()
    {
        super.onResume();




        TextView tv_sv_streetName=(TextView)getView().findViewById(R.id.tv_sv_streetName);

        if(street!=null) {
            tv_sv_streetName.setText(street.toString());
        }
    }

}
