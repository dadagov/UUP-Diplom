package com.example.magomeddadagov.uup.Model;

import com.example.magomeddadagov.uup.R;
import com.orm.SugarRecord;

/**
 * Created by magomeddadagov on 16.11.16.
 */

public class OrgObject extends SugarRecord {

    String name;
    long addressId;
    String ownerName;
    String phone;
    long typeId;
    String securityInfo;
    String otherInfo;

    public  String getName(){return  name;}
    public String getOwnerName(){return ownerName;}
    public  Address getAddress(){return  Address.findById(Address.class,addressId);}



    public  OrgObject(){}
    public  OrgObject(String name, Address address, String ownerName, String phone, ObjectType type, String securityInfo, String otherInfo)
    {
      this.name=name;
        this.addressId=address.getId();
        this.ownerName=ownerName;
        this.phone=phone;
        this.typeId=type.getId();
        this.securityInfo=securityInfo;
        this.otherInfo=otherInfo;

    }

    @Override
    public  String toString()
    {
        return name;
    }


}
