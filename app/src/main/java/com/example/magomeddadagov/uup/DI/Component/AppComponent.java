package com.example.magomeddadagov.uup.DI.Component;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.example.magomeddadagov.uup.Fragment.Street.StreetViewFragment;
import com.example.magomeddadagov.uup.MainActivity;

import dagger.Component;

/**
 * Created by Dadagov on 16.01.2017.
 */

@Component()
public interface AppComponent {
    void injectServices(StreetViewFragment fragment);
}
