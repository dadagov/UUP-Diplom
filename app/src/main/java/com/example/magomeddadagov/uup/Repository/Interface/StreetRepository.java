package com.example.magomeddadagov.uup.Repository.Interface;

import com.example.magomeddadagov.uup.Entity.Street;

public interface StreetRepository extends Repository<Street,Long> {
}
