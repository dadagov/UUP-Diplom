package com.example.magomeddadagov.uup.Fragment.People;


import android.app.DatePickerDialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.magomeddadagov.uup.Model.Address;
import com.example.magomeddadagov.uup.Model.People;
import com.example.magomeddadagov.uup.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class PeopleViewFragment extends Fragment {

    People people;
    Boolean enable;
    Boolean isEditMode=false;



     EditText tv_pv_name;
     EditText tv_pv_surname;
     EditText tv_pv_middlename;
     EditText tv_pv_birthDate;
     EditText tv_pv_birthPlace;
     EditText tv_pv_address;

     Button btn_peopleViewEditSave;




    public PeopleViewFragment() {
        this.people=new People();
        this.isEditMode=true;

    }

    public PeopleViewFragment(People people)
    {
        this.people=people;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_people_view, container, false);


        tv_pv_name=(EditText) view.findViewById(R.id.tv_pv_name);
        tv_pv_surname=(EditText) view.findViewById(R.id.tv_pv_surname);
        tv_pv_middlename=(EditText) view.findViewById(R.id.tv_pv_middlename);
        tv_pv_birthDate=(EditText) view.findViewById(R.id.tv_pv_birthDate);
        tv_pv_birthPlace=(EditText) view.findViewById(R.id.tv_pv_birthPlace);
        tv_pv_address=(EditText) view.findViewById(R.id.tv_pv_address);
        btn_peopleViewEditSave=(Button)view.findViewById(R.id.btn_peopleViewEditSave);

        setEnableViews(isEditMode);


        return view;
    }

    @Override
    public  void onResume()
    {
        super.onResume();

        tv_pv_name.setText(people.getName());
        tv_pv_surname.setText(people.getSurname());
        tv_pv_middlename.setText(people.getMiddlename());
        tv_pv_birthDate.setText(DateUtils.formatDateTime(getContext(),people.getBirthDate(),DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR));
        tv_pv_birthPlace.setText(people.getBirthPlace());
        tv_pv_address.setText(people.getAddress()!=null?people.getAddress().toString():"");



///////
        tv_pv_birthDate. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar=Calendar.getInstance();

                DatePickerDialog dialog=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                        calendar.set(year,monthOfYear,dayOfMonth);
                        people.setBirthDate(calendar.getTimeInMillis());

                        String s = DateUtils.formatDateTime(getContext(), calendar.getTimeInMillis(),

                                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR);

                        tv_pv_birthDate.setText(s);
                    }
                },
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)

                );
                dialog.show();
            }
        });

///////



        btn_peopleViewEditSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isEditMode)
                {
                   isEditMode=false;
                    setEnableViews(isEditMode);
                    save();



                }
                else {

                    isEditMode=true;
                    setEnableViews(isEditMode);


                }
            }
        });



            tv_pv_address.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                    builder.setTitle("Выберите адрес");

                    List<Address> addresses = Address.listAll(Address.class);
                    final ArrayMap<String,Address> addressMap=new ArrayMap<String, Address>();

                    for (Address address:addresses)
                    {
                        addressMap.put(address.toString(),address);
                    }

                    final String[] strings = new String[addressMap.size()];
                    for (int i=0; i<addresses.size(); i++)
                    {
                        Address adr= addresses.get(i);
                        strings[i]=adr.toString();
                        addressMap.put(adr.toString(),adr);

                    }



                    builder.setItems(strings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            tv_pv_address.setText(strings[i]);
                            people.setAddress(addressMap.get(strings[i]));

                        }
                    });

                    builder.show();
                }
            });

    }


    void setEnableViews(Boolean flag)
    {
        tv_pv_name.setEnabled(flag);
        tv_pv_name.setCursorVisible(flag);

         tv_pv_surname.setEnabled(flag);
        tv_pv_surname.setCursorVisible(flag);

         tv_pv_middlename.setEnabled(flag);
        tv_pv_middlename.setCursorVisible(flag);

         tv_pv_birthDate.setEnabled(flag);
        tv_pv_birthDate.setCursorVisible(flag);

         tv_pv_birthPlace.setEnabled(flag);
        tv_pv_birthPlace.setCursorVisible(flag);

         tv_pv_address.setEnabled(flag);
        tv_pv_address.setCursorVisible(false);

        if (!flag)
        {
            btn_peopleViewEditSave.setText("Изменить");
        }
        else {
            btn_peopleViewEditSave.setText("Сохранить");
        }



    }

    void save()
    {
         people.setName(tv_pv_name.getText().toString());
         people.setSurname(tv_pv_surname.getText().toString());
        people.setMiddlename( tv_pv_middlename.getText().toString());

        people.setBirthPlace (tv_pv_birthPlace.getText().toString());


        people.save();


    }



}
